package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {

        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        boolean playing = true;
        while (playing) {
        System.out.println("Let's play round " + roundCounter);
        String inputFromUser = readInput("Your choice (Rock/Paper/Scissors)?");
        if(validInput(inputFromUser)){
            String computerChoice = randomFromList();
            boolean isTie = inputFromUser.equals(computerChoice);
            String tieText = isTie ? ". It's a tie" : "";
            System.out.println("Human chose " + inputFromUser + ". computer chose " + computerChoice + tieText);
            boolean winner = is_winner(inputFromUser, computerChoice);
            if (!isTie) { 
             if(winner){
                humanScore += 1;
            } else {
                computerScore += 1;
            }
          }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String continuePlaying = readInput("Do you wish to continue playing? (y/n)?");
            roundCounter += 1;
            if (continuePlaying.equals("n")){
                System.out.println("Bye bye :)");
                playing = false;
            
            }
        } else {
            System.out.println("I don't understand " + inputFromUser + " Try again");
        }
 
    }
       
    }

    private boolean validInput(String humanChoice) {
        if (rpsChoices.contains(humanChoice.toLowerCase())) {
            return true;
        }       
        else {
            return false;
        }
    }
    private String randomFromList() {
        Random rand = new Random();
        return rpsChoices.get(rand.nextInt(rpsChoices.size()));
    }

    private boolean is_winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2 == "rock";
        }
         else if (choice1.equals("scissors"))
         {
            return choice2.equals("paper");
         }
            
        else {
            return choice2.equals("scissors");
        }
           
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
